import java.util.ArrayList;

public class ZooAnimal 
{

    // variable creation
    String name;
    String food;
    String animalType;
    String description;
    String environment;


    // animal object creation
    public ZooAnimal(String name, String description, String animalType, String food, String environment) 
    {
        this.name = name;
        this.description = description;
        this.animalType = animalType;
        this.food = food;
        this.environment = environment;
    }
    public static void main(String[] args) 
    {
        ArrayList<ZooAnimal> arrayOfAnimalsInZoo = new ArrayList<>();
        arrayOfAnimalsInZoo.add(new ZooAnimal("Puma", "They are the worlds's fourth largest cat. Their fur is as black as the night.",
         "They are Mammals.", "They eat medium and large sized mammals like deer.", "They prefer to live in areas like desert scrubs, chaparral, swamps, and forest. They can be found all over South and North America."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("Bats", "They are typically small and come in several colors such as black, brown, red, tan, and grey. They have short snouts, large ears, and covered in fur.",
         "They are mammals.", "They eat night flying insects.", "They are found in caves, rock crevices, old buildings, bridges, mines, and trees."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("Swordfish", "An elongated, scaleless fish, has a dorsel fin, and a long sword.",
         "They are ocean creatures.", "They eat a variety of fish and invertebrates such as squid.", "They are found in tropical, temperate, and sometimes cold waters of the Atlantic, Indian, and Pacific Oceans."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("Tigers", "They are the largest and most powerful of all the cats . They have orange fur with black stripes, and white markings .",
         "They are Mammals.", "They mainly eat deer, wild pigs, water buffalo, and antelope .", "They are found in environments such as swamps, grasslands, and rainforest."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("Bear", "They have large bodies, stocky legs, long snout, shaggy fur,and a short tail. They can run and climb very fast. Their coat color is brown.",
         "They are Mammals.", "They eat grasses, roots, berries, and insects. They also eat fish and mammals .", "They are found  throughout North and South America, Europe, and Asia."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("hawk", "They are strong, powerful birds. Curved talons for capturing prey, and their strong beaks are hooked for biting.",
         "They are birds.", "They eat rabbits, rats, voles, and snakes.", "They inhabit places like deserts, and fields."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("Frog", "They have protruding eyes, no tail, and strong webbed feet for jumping and swimming.",
         "They are Amphibians.", "They eat insects such as snails, slugs, and worms.", "They are found in tropical forest to frozen tundras tp deserts."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("Giraffes", "They have are the world's tallest mammals with long legs and a long neck.",
         "They are Mammals.", "They eat plants, grass, and leaves from tall trees.", "They live in both semi-arid savannah and savannah woodlands."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("flamingos", "They have slender legs, long necks, large wings, and short tails.",
         "They are Wading Birds.", "They eat algae, small seeds,fly larvae, and other plants and animals that live in shallow waters.", "They are found in South America, Middle East and the Caribbean."));
         arrayOfAnimalsInZoo.add(new ZooAnimal("Zebra", "They have black fur with white stripes and predominantly white bellies.",
         "They are Mammals.", "They eat mostly grasses and sometimes leaves and stems of bushes.", "They live in areas such as grasslands and savanna woodlands."));

         for (ZooAnimal zooAnimal: arrayOfAnimalsInZoo)
         {
            System.out.println(zooAnimal.name + " Description: " + zooAnimal.description + "\nAnimal Type: " + zooAnimal.animalType + "\nFood: " + zooAnimal.food + "\nEnvironment: " + zooAnimal.environment + "\n\n");
         }

    }
}